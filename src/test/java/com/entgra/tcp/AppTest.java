package com.entgra.tcp;

import com.entgra.tcp.utils.Utils;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;


/**
 * Unit test for simple App.
 */
public class AppTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest(String testName) {
        super(testName);
    }
    
    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(AppTest.class);
    }
    
    /**
     * Rigourous Test :-)
     */
    public void testApp() {
        assertTrue(true);
    }
    
    public void test1(){
        DateFormat formatter = new SimpleDateFormat("hh:mm:ss");
        String time = formatter.format(new Date());
    
        String[] decTime = time.split(":");
        StringBuilder decString = new StringBuilder();
        for (String dec:decTime) {
            decString.append(dec);
        }
        String dec = decString.toString();
        
        byte[] hexTime = dec.getBytes();
        StringBuilder hexString = new StringBuilder();
        for (byte hex:hexTime) {
            hexString.append(Integer.toHexString(hex));
        }
        System.out.println(hexString);
    
    }
    
    public void testBCCAddition(){
        
        IECReader iecReader = new IECReader();
        String tempHex = "015030022839364132464546302903";
        String dateHex = IECReader.getDateHex(IECReader.getDate());
        String res = "0228" + dateHex + "2903";
        byte[] arr = Utils.hexStringToByteArray(tempHex);
        byte bcc = iecReader.generateChecksum(arr);
        byte[] arr2 = Utils.addBCC(arr.length, arr, bcc);
        System.out.println(Arrays.toString(arr));
        System.out.println(Arrays.toString(arr2));
        System.out.println(IECReader.bytesToHex(arr2));
        System.out.println(bcc);
        
    }
}

package com.entgra.tcp;

import java.io.IOException;

public interface Medium {
    
    void openConnection() throws IOException;
    
    void closeConnection() throws IOException;
    
    void writeBuffer(byte[] buffer) throws IOException;
    
    byte[] readBytes() throws IOException;
    
    boolean isInputBufferAvailable() throws IOException;
    
    String getDeviceAddress();
    
    boolean isConnected();
    
}

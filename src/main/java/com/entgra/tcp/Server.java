package com.entgra.tcp;

import java.net.*;
import java.io.*;
import java.util.Arrays;
import java.util.Date;

public class Server {
    
    public Server(String serverAddress, int port) {
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println("Server started");
            
            System.out.println("Waiting for a client ...");
            
            Socket socket = serverSocket.accept();
            System.out.println("Client accepted");
            
            InputStream inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream();
            
            IECReader iecReader = new IECReader();
            iecReader.setServerAddress(serverAddress);
            iecReader.setPort(port);
            iecReader.setDataInputStream(new DataInputStream(inputStream));
            iecReader.setDataOutputStream(new DataOutputStream(outputStream));
            iecReader.setSocket(socket);
            IECReader.setDate(new Date());
            IECReader.setPassword("12345600");
            
            String reading = "";
            while (!reading.equals("over")) {
                try {
                    byte[] received = iecReader.receive(false, true);
                    reading = new String(received).substring(1, received.length - 2);
                    
                    System.out.println("Request : " + reading);
                    for (int i = 0; i < Response.responses.size(); i++) {
                        if (Arrays.equals(reading.getBytes(), Response.responses.get(i).getRequest())) {
                            iecReader.send(Response.responses.get(i).getResponse());
                            System.out.println("Response : " + IECReader.bytesToHex(Response.responses.get(i).getResponse()));
                            break;
                        }
                    }
                } catch (Exception ex) {
                    System.out.println(ex);
                }
                
            }
            System.out.println("Closing connection");
            
            socket.close();
        } catch (IOException i) {
            System.out.println(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args) {
        new Server("localhost", 8071);
    }
}


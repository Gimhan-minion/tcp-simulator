package com.entgra.tcp;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class TCPMedium implements Medium {
    
    private final String serverAddress;
    private final int port;
    private DataInputStream dataInputStream;
    private DataOutputStream dataOutputStream;
    private Socket socket;
    private boolean isConnected;
    
    public TCPMedium(String serverAddress, int port, DataInputStream dataInputStream, DataOutputStream dataOutputStream, Socket socket) {
        this.serverAddress = serverAddress;
        this.port = port;
        this.dataInputStream = dataInputStream;
        this.dataOutputStream = dataOutputStream;
        this.socket = socket;
    }
    
    @Override
    public void openConnection() throws IOException {
        if (socket == null || !socket.isConnected()) {
            System.out.println("Connecting to " + serverAddress + " on port " + port);
            socket = new Socket(serverAddress, port);
            System.out.println("Successfully connected to " + serverAddress);
        }
        
        InputStream inputStream = socket.getInputStream();
        dataInputStream = new DataInputStream(inputStream);
        
        OutputStream outputStream = socket.getOutputStream();
        dataOutputStream = new DataOutputStream(outputStream);
        isConnected = true;
    }
    
    @Override
    public void closeConnection() throws IOException {
        if (socket != null && socket.isConnected()) {
            socket.close();
        }
        socket = null;
        isConnected = false;
    }
    
    @Override
    public void writeBuffer(byte[] buffer) throws IOException {
        dataOutputStream.write(buffer);
    }
    
    @Override
    public byte[] readBytes() throws IOException {
        if (isInputBufferAvailable()) {
            byte[] buffer = new byte[dataInputStream.available()];
            dataInputStream.readFully(buffer);
            return buffer;
        } else {
            return new byte[0];
        }
    }
    
    @Override
    public boolean isInputBufferAvailable() throws IOException {
        return dataInputStream.available() > 0;
    }
    
    @Override
    public String getDeviceAddress() {
        return serverAddress;
    }
    
    @Override
    public boolean isConnected() {
        return isConnected;
    }
    
}

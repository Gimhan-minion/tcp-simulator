package com.entgra.tcp;

import com.entgra.tcp.utils.Utils;

import java.util.ArrayList;

public class Response {
    private int id;
    private byte[] request;
    private byte[] response;
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public byte[] getRequest() {
        return request;
    }
    
    public void setRequest(byte[] request) {
        this.request = request;
    }
    
    public byte[] getResponse() {
        return response;
    }
    
    public void setResponse(byte[] response) {
        this.response = response;
    }
    
    public static ArrayList<Response> responses = new ArrayList<>();
    
    static {
        String res, req;
        
        Response response = new Response();
        response.setId(0);
        response.setRequest("?000000000001!".getBytes());
        response.setResponse(Utils.hexStringToByteArray("2F414E5433534C30352D54454330332D312E300D0A"));
        responses.add(response);
    
        response = new Response();
        response.setId(1);
        response.setRequest("031".getBytes());
        response.setResponse(Utils.calAddBCC("015030022839364132464546302903"));
        responses.add(response);
    
        response = new Response();
        response.setId(2);
        String password = IECReader.getPassword();
        String request = "P1\u0002(" + password + ")";
        response.setRequest(request.getBytes());
        response.setResponse(Utils.hexStringToByteArray("06"));
        responses.add(response);
        
        response = new Response();
        response.setId(3);
        String dateHex = IECReader.getDateHex(IECReader.getDate());
        res = "0228" + dateHex + "2903";
        System.out.println(dateHex);
        response.setRequest("R1\u00021.0.0.9.2()".getBytes());
        response.setResponse(Utils.calAddBCC(res));
        responses.add(response);
        
        response = new Response();
        response.setId(4);
        String timeHex = IECReader.getTimeHex(IECReader.getDate());
        res = "0228"+ timeHex + "290302";
        response.setRequest("R1\u00021.0.0.9.1()".getBytes());
        response.setResponse(Utils.calAddBCC(res));
        responses.add(response);
        
        response = new Response();
        response.setId(5);
        timeHex = IECReader.getTimeHex(IECReader.getDate());
        req = "W1\u00021.0.0.9.1(" + timeHex + ")";
        response.setRequest(req.getBytes());
        response.setResponse(Utils.hexStringToByteArray("0228303030303137373130343032290304"));
        responses.add(response);
    }
}

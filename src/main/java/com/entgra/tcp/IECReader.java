package com.entgra.tcp;

import com.google.common.primitives.Bytes;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IECReader {
    protected static final String R5 = "R5";
    protected static final byte CR = 13;
    protected static final byte LF = 10;
    protected static final byte ACK = 6;
    protected static final byte NAK = 21;
    protected static final byte SOH = 1;
    protected static final byte STX = 2;
    protected static final byte ETX = 3;
    protected static final String P1 = "P1";
    protected static final String R1 = "R1";
    protected static final String W1 = "W1";
    protected static final String B0 = "B0";
    protected static final String ACK_OPTION = "031";
    protected static final String READ_ALL_ACK_OPTION = "030";
    protected static final String E2 = "E2";
    protected static final Pattern regex = Pattern.compile("\\((.*?)\\)");
    protected static final DecimalFormat df = new DecimalFormat();
    protected final byte[] CR_LF = new byte[]{CR, LF};
    
    public static String password = "";
    public static Date date = new Date();
    
    public String serverAddress;
    public int port;
    public DataInputStream dataInputStream;
    public DataOutputStream dataOutputStream;
    public Socket socket;
    
    public String getServerAddress() {
        return serverAddress;
    }
    
    public void setServerAddress(String serverAddress) {
        this.serverAddress = serverAddress;
    }
    
    public int getPort() {
        return port;
    }
    
    public void setPort(int port) {
        this.port = port;
    }
    
    public DataInputStream getDataInputStream() {
        return dataInputStream;
    }
    
    public void setDataInputStream(DataInputStream dataInputStream) {
        this.dataInputStream = dataInputStream;
    }
    
    public DataOutputStream getDataOutputStream() {
        return dataOutputStream;
    }
    
    public void setDataOutputStream(DataOutputStream dataOutputStream) {
        this.dataOutputStream = dataOutputStream;
    }
    
    public Socket getSocket() {
        return socket;
    }
    
    public void setSocket(Socket socket) {
        this.socket = socket;
    }
    
    public static String getPassword() {
        return password;
    }
    
    public static Date getDate() {
        return date;
    }
    
    public static void setPassword(String password) {
        IECReader.password = password;
    }
    
    public static void setDate(Date date) {
        IECReader.date = date;
    }
    
    TCPMedium medium;
    
    
    public static String bytesToHex(byte[] bytes) {
        if (bytes != null && bytes.length != 0) {
            char[] hexArray = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
            char[] hexChars = new char[bytes.length * 3];
            
            for (int pos = 0; pos != bytes.length; ++pos) {
                int tmp = bytes[pos] & 255;
                hexChars[pos * 3] = hexArray[(byte) (tmp >>> 4)];
                hexChars[pos * 3 + 1] = hexArray[(byte) (tmp & 15)];
                hexChars[pos * 3 + 2] = ' ';
            }
            
            return new String(hexChars, 0, hexChars.length - 1);
        } else {
            return "";
        }
    }
    
    protected byte generateChecksum(List<Byte> input) {
        int checkSum = 0;
        for (int i = 1; i < input.size(); i++) {
            checkSum ^= input.get(i) & 0xFF;
        }
        return (byte) checkSum;
    }
    
    public byte generateChecksum(byte[] input) {
        int checkSum = 0;
        for (int i = 1; i < input.length; i++) {
            checkSum ^= input[i] & 0xFF;
        }
        return (byte) checkSum;
    }
    
    
    public byte[] receive(boolean requireETX, boolean ignoreBCC) throws Exception {
        medium = new TCPMedium(serverAddress, port, dataInputStream, dataOutputStream, socket);
        long startTS;
        boolean etxReceived = !requireETX;
        List<Byte> reading = new ArrayList<>();
        
        do {
            startTS = System.currentTimeMillis();
            while (!medium.isInputBufferAvailable()) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ignored) {
                }
                if (System.currentTimeMillis() > startTS + 30000) {
                    String msg = "Receive timed out";
                    System.out.println(msg);
                    throw new RuntimeException(msg);
                }
            }
            byte[] data = medium.readBytes();
            for (byte b : data) {
                reading.add(b);
                if (b == ETX) {
                    etxReceived = true;
                }
            }
            
        } while (!etxReceived);
        if (reading.size() > 2 && reading.get(reading.size() - 2) == ETX) {
            List<Byte> dataBytes = reading.subList(0, reading.size() - 1);
            byte bcc = generateChecksum(dataBytes);
            System.out.println(bcc);
            if (bcc != reading.get(reading.size() - 1)) {
                String msg = "BCC validation failed. Expected: " + bcc + ", received:" +
                        reading.get(reading.size() - 1);
                System.out.println(msg);
                if (!ignoreBCC) {
                    throw new Exception(msg);
                }
            }
            
            Matcher regexMatcher = regex.matcher(new String(Bytes.toArray(reading)));
            if (regexMatcher.find()) {
                String errorCode = regexMatcher.group(1);
                if (errorCode != null && errorCode.startsWith("ER") && errorCode.length() == 4) {
                    throw new Exception(errorCode);
                }
            }
        }
        return Bytes.toArray(reading);
    }
    
    protected void send(byte[] buffer) throws IOException {
        medium.writeBuffer(buffer);
        
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ignored) {
        }
    }
    
    public static String getTimeHex(Date date){
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        String time = formatter.format(date);
    
        String[] decTime = time.split(":");
        return getHexString(decTime);
    }
    
    public static String getDateHex(Date date){
        DateFormat formatter = new SimpleDateFormat("yy-MM-dd");
        String time = formatter.format(date);
        
        String[] decTime = time.split("-");
        return getHexString(decTime);
    }
    
    private static String getHexString(String[] decTime) {
        StringBuilder decString = new StringBuilder();
        for (String dec:decTime) {
            decString.append(dec);
        }
        String dec = decString.toString();
        
        byte[] hexTime = dec.getBytes();
        StringBuilder hexString = new StringBuilder();
        for (byte hex:hexTime) {
            hexString.append(Integer.toHexString(hex));
        }
        return String.valueOf(hexString);
    }
    
}

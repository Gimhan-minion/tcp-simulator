package com.entgra.tcp.utils;

import com.entgra.tcp.IECReader;

public class Utils {
    public static byte[] hexStringToByteArray(String str){
        byte[] val = new byte[str.length() / 2];
        for (int i = 0; i < val.length; i++) {
            int index = i * 2;
            int j = Integer.parseInt(str.substring(index, index + 2), 16);
            val[i] = (byte) j;
        }
        return val;
    }
    
    public static byte[] addBCC(int n, byte[] arr, byte x)
    {
        int i;
        byte[] newarr = new byte[n + 1];
        for (i = 0; i < n; i++)
            newarr[i] = arr[i];
        newarr[n] = x;
        return newarr;
    }
    
    public static byte[] calAddBCC(String res){
        IECReader iecReader = new IECReader();
        byte[] arr = hexStringToByteArray(res);
        byte bcc = iecReader.generateChecksum(arr);
        return addBCC(arr.length, arr, bcc);
    }
    
}
